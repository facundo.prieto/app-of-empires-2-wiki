import {createStackNavigator} from 'react-navigation-stack'
import {createAppContainer} from 'react-navigation'
import { ImageBackground } from "react-native"
import React, { Component } from "react"
import IndexScreen from './src/screens/IndexScreen'
import CivilizationsScreen from './src/screens/CivilizationsScreen'
import CivilizationScreen from './src/screens/CivilizationScreen'
import UnitsScreen from './src/screens/UnitsScreen'
import UnitScreen from './src/screens/UnitScreen'
import StructuresScreen from './src/screens/StructuresScreen'
import StructureScreen from './src/screens/StructureScreen'
import TechnologiesScreen from './src/screens/TechnologiesScreen'
import TechnologyScreen from './src/screens/TechnologyScreen'
import { LogBox } from 'react-native';

LogBox.ignoreLogs(['Warning: ...']); // Ignore log notification by message
LogBox.ignoreAllLogs();//Ignore all log notifications

const navigator = createStackNavigator({
  WELCOME: IndexScreen,
  CIVILIZATIONS: CivilizationsScreen,
  CIVILIZATION: CivilizationScreen,
  UNITS: UnitsScreen,
  UNIT: UnitScreen,
  STRUCTURES: StructuresScreen,
  STRUCTURE: StructureScreen,
  TECHNOLOGIES: TechnologiesScreen,
  TECHNOLOGY: TechnologyScreen
},{
  initialRouteName: 'WELCOME',
  headerMode: 'none',
  transparentCard: true,
});

const AppNavigator = createAppContainer(navigator);

export class App extends Component {
  render() {
    return (
      <ImageBackground style={{width: '100%',height: '100%'}} source={require("./assets/background/Fondo1.png")}>
        <AppNavigator />
      </ImageBackground>
    );
  }
}

export default App;