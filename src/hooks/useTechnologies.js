import {useEffect, useState} from 'react'
import ageApi from '../api/ageApi'

export default () => {
    const [results, setResults] = useState([])
    const [errorMessage, setErrorMessage] = useState('')

    // Getting the values from api
    const searchApi = async (searchTerm)=>{
        try{
            const response = await ageApi.get('/technologies')
            setResults(response.data.technologies)
        } catch(err){
            setErrorMessage('Algo salio mal.')
        }
    }

    useEffect(() => {
        searchApi(' ')
    }, [])

    return [searchApi, results, errorMessage]
}