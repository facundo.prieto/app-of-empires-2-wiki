import React from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';
import ImageCollection from '../../assets/structures/ImageCollection.js';
import StyledText from "../components/StyledText";

const StructuresDetail = ({result}) =>{
    return(
        <View style={styles.container}>
            <Image style={styles.image} source={ImageCollection[result.id]}/>
            <StyledText textoDetail={result.name} />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flexDirection: "row",
        padding: 5,
        alignItems: 'center'
    },
    image: {
        width: 90,
        height: 90,
        borderRadius: 4,
        marginBottom: 5
    },
    text:{
        fontSize: 30,
        padding: 30
    }
})

export default StructuresDetail