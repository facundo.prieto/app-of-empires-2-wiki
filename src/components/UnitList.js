import React from 'react'
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native'
import UnitDetail from '../components/UnitDetail'


const UnitList = ({title, results, navigation}) =>{

    if(!results.length){
        return null;
    }
    
    return(
        <View style={styles.container}>
            <FlatList 
                data={results}
                keyExtractor={(results) => results.id.toString()}
                renderItem={({item}) => {
                    return (
                        <TouchableOpacity onPress={() =>navigation.navigate('UNIT', {id: item.id})}>
                            <UnitDetail result={item}/>
                        </TouchableOpacity>
                    )
                }}
            >
            </FlatList>
        </View>
    )
}


const styles = StyleSheet.create({
    container:{
        marginBottom: 40,
        marginTop: 40,
        marginHorizontal: 20
    }
})

export default UnitList