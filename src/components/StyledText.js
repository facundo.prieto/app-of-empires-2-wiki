import React, { Component } from "react";
import { Text, StyleSheet } from "react-native";
import * as Font from 'expo-font';

export class StyledText extends Component {
    state = {
      fontsLoaded: false,
    };
    
    async loadFonts() {
        await Font.loadAsync({
            Junge: require('../../assets/fonts/Junge/Junge-Regular.ttf'),
            Marcellus: require('../../assets/fonts/Marcellus_SC/MarcellusSC-Regular.ttf')
        });
        this.setState({ fontsLoaded: true });
    }
        
    async componentDidMount() {
        this.loadFonts();
    }

  render() {
    if (this.state.fontsLoaded) {    //al ser async no hay que renderizar nada hasta que no se carguen
        if(this.props.textoIndex){
            return (
                <Text style={styles.textoIndex}>{this.props.textoIndex}</Text>
            );
        }
        if(this.props.textoDetail){
            return (
                <Text style={styles.textoDetail}>{this.props.textoDetail}</Text>
            );
        }
        if(this.props.textoTitleElemnt){
            return (
                <Text style={styles.textoTitleElemnt}>{this.props.textoTitleElemnt}</Text>
            );
        }
        if(this.props.textoElemnt){
            return (
                <Text style={styles.textoElemnt}>{this.props.textoElemnt}</Text>
            );
        }
    } else {
        return null;
    }
  }
}

const styles = StyleSheet.create({
    textoIndex: {
        fontSize: 29,
        padding: 20,
        alignSelf: 'center', 
        fontFamily: 'Marcellus',
        color: 'white'
    },
    textoDetail: {
        fontSize: 25,
        padding: 30,
        fontFamily: 'Junge'
    },
    textoElemnt: {
        padding: 8,
        fontSize:20,
        fontFamily: 'Junge'
    },
    textoTitleElemnt: {
        borderRadius: 4,
        paddingHorizontal:5,
        backgroundColor: 'rgba(224, 223, 220, .3)',
        marginHorizontal: 15,
        marginBottom: 10,
        fontSize:25,
        fontFamily: 'Marcellus'
    }
});

export default StyledText; 
