import React, { useState, useEffect } from 'react'
import {View, Text, StyleSheet, ActivityIndicator, Image, ScrollView} from 'react-native'
import ageApi from '../api/ageApi'
import StyledText from "../components/StyledText"

const StructureScreen = ({ navigation}) =>{
    const [result, setResult] = useState([])
    const id =navigation.getParam('id')
    
    const getStructure = async (id) =>{
        const response = await ageApi.get(`/structure/${id}`)
        setResult(response.data)
    }

    useEffect(() => {
        getStructure(id)
    }, [])
    
    if(!result){
        return null
    }

    return(
        <View style={styles.container}>
            {result.id ?
                <View style={styles.scrollcontainer}>
                    <ScrollView>
                        <StyledText textoTitleElemnt={result.name}/>
                        <View>
                            {result.description ?       
                                <StyledText textoElemnt={'  Description: ' + result.description}/>
                            : null}
                            {result.expansion ?
                                <StyledText textoElemnt={'  Expansion: ' + result.expansion}/>
                            : null}
                            {result.age ?
                                <StyledText textoElemnt={'  Age: ' + result.age}/>
                            : null}
                            {result.build_time ?
                                <StyledText textoElemnt={'  Build time: ' + result.build_time}/>
                            : null}
                            {result.hit_points ?
                                <StyledText textoElemnt={'  Hit points: ' + result.hit_points}/>
                            : null}
                            {result.line_of_sight ?
                                <StyledText textoElemnt={'  Line of sight: ' + result.line_of_sight}/>
                            : null}
                            {result.armor ?
                                <StyledText textoElemnt={'  Armor: ' + result.armor}/>
                            : null}
                            {result.range ?
                                <StyledText textoElemnt={'  Range: ' + result.range}/>
                            : null}
                            {result.reload_time ?
                                <StyledText textoElemnt={'  Reload time: ' + result.reload_time}/>
                            : null}
                            {result.attack ?
                                <StyledText textoElemnt={'  Attack: ' + result.attack}/>
                            : null}

                            {result.cost ?
                                <StyledText textoElemnt={'  Cost: '}/>
                            : null}
                            {result.cost && result.cost.Wood ? 
                                <StyledText textoElemnt={'        o Wood: ' + result.cost.Wood}/>
                            : null}
                            {result.cost && result.cost.Food ? 
                                <StyledText textoElemnt={'        o Food: ' + result.cost.Food}/>
                            : null}
                            {result.cost && result.cost.Stone ? 
                                <StyledText textoElemnt={'        o Stone: ' + result.cost.Stone}/>
                            : null}
                            {result.cost && result.cost.Gold ? 
                                <StyledText textoElemnt={'        o Gold: ' + result.cost.Gold}/>
                            : null}
                            {result.cost && result.cost.info ? 
                                <StyledText textoElemnt={'        o Info: ' + result.cost.info}/>
                            : null}

                            {result.special ?
                                <StyledText textoElemnt={'  Special:'}/>
                            : null}
                            {result.special ? 
                                result.special.map((_special,_index) => {
                                    return(
                                        <StyledText key={_index} textoElemnt={'        o ' + _special}/>
                                    )
                                })
                            : null}
                        </View> 
                    </ScrollView>
                </View> 
            :
                <View style={[styles.container,styles.vertical]}>
                    <Image source={require('../../assets/wololo.gif')}/>
                    <Text> {`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`} </Text>
                    <ActivityIndicator size="large" color="#EA3109" />
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: "center",
        marginBottom: 40,
        marginHorizontal: 20,
    },
    scrollcontainer:{
        marginTop:80,
        height: '80%',
        marginHorizontal: 20,
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
    vertical: {
        flexDirection: "column",
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
})


export default StructureScreen