import React, { Component } from "react";
import { ScrollView, StyleSheet, TouchableOpacity, ImageBackground, Image, Button } from "react-native";
import { Audio } from "expo-av";
import StyledText from "../components/StyledText";

export class IndexScreen extends Component {
  constructor(){
      super();
      this.state = {
        sonando: true //para que no se reproduzca automaticamente esto va en false
      }
  }

  async componentDidMount() {
    Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DUCK_OTHERS,
      shouldDuckAndroid: true,
      staysActiveInBackground: false,
      playThroughEarpieceAndroid: true,
    });

    this.sound = new Audio.Sound();

    const status = {
      shouldPlay: true, //para que no se reproduzca automaticamente esto va en false
    };

    this.sound.loadAsync(require("../../assets/song/1min_original.mp3"), status, false);
  }

  async soundOnOff(){
    let estado = await this.sound.getStatusAsync();
    // console.log(estado)
    if(estado.isPlaying){
      this.sound.pauseAsync();
      this.setState({sonando: false});
    }else{
      this.sound.playAsync();
      this.setState({sonando: true});
    }
  }

  render() {
    let navigation = this.props.navigation;

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center', alignItems: 'center' }} style={styles.container}>
          <Image style={styles.image} source={require('../../assets/AoE.png')}/>

          <TouchableOpacity style={styles.category} onPress={() =>navigation.navigate("CIVILIZATIONS", { navigation: navigation })}>
            <ImageBackground style={{}} source={require("../.././assets/button/Boton2.png")}>
              <StyledText textoIndex={"Civilizations"} />
            </ImageBackground>
          </TouchableOpacity>
          <TouchableOpacity style={styles.category} onPress={() =>navigation.navigate("UNITS", { navigation: navigation })}>
            <ImageBackground style={{}} source={require("../.././assets/button/Boton2.png")}>
              <StyledText textoIndex={"Units"} />
            </ImageBackground>
          </TouchableOpacity>
          <TouchableOpacity style={styles.category} onPress={() =>navigation.navigate("STRUCTURES", { navigation: navigation })}>
            <ImageBackground style={{}} source={require("../.././assets/button/Boton2.png")}>
              <StyledText textoIndex={"Structures"} />
            </ImageBackground>
          </TouchableOpacity>
          <TouchableOpacity style={styles.category} onPress={() =>navigation.navigate("TECHNOLOGIES", { navigation: navigation })}>
            <ImageBackground style={{}} source={require("../.././assets/button/Boton2.png")}>
              <StyledText textoIndex={"Technologies"} />
            </ImageBackground>
          </TouchableOpacity>
          <StyledText textoIndex={" "} />
          <TouchableOpacity style={styles.boton} onPress={() =>this.soundOnOff()}>
            {this.state.sonando ? 
              <Image source={ require('../../assets/soundOn.png')}  style={styles.sound}/> 
            : <Image source={ require('../../assets/soundOff.png')}  style={styles.sound}/>}
          </TouchableOpacity>
        </ScrollView> 
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 30,
  },
  category: {
    width: '70%'
  },
  sound: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain'
  },
  boton: {
    width: 50,
    height: 50,
  },
  image: {
    marginBottom: 50,
    width: '80%',
    height: '25%'
  },
   
});

export default IndexScreen;