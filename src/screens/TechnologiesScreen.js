import React, { useState, useEffect } from 'react'
import {View, Text, StyleSheet, ScrollView, ActivityIndicator, Image} from 'react-native'
import TechnologiesList from '../components/TechnologiesList'
import useTechnologies from '../hooks/useTechnologies'

const SearchScreen = ({navigation}) =>{
    const [searchApi, technologies, errorMessage] = useTechnologies();
    
    return(
        <View style={{flex:1}}>
            {errorMessage ? <Text>{errorMessage}</Text> : null}
            {technologies.length > 0 ?
                <TechnologiesList results={technologies} title='TECHNOLOGIES' navigation={navigation}/>
            :
                <View style={[styles.container,styles.vertical]}>
                    <Image source={require('../../assets/wololo.gif')}/>
                    <Text> {`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`} </Text>
                    <ActivityIndicator size="large" color="#EA3109" />
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center"
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 100,
        flex:0.5
    },
    vertical: {
        flexDirection: "column",
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default SearchScreen