import React, { useState, useEffect } from 'react'
import {ScrollView, View, Text, StyleSheet, ActivityIndicator, Image} from 'react-native'
import ageApi from '../api/ageApi'
import StyledText from "../components/StyledText"

const ResultShowScreen = ({ navigation}) =>{
    const [result, setResult] = useState([])
    const id =navigation.getParam('id')
    
    const getResult = async (id) =>{
        const response = await ageApi.get(`/unit/${id}`)
        setResult(response.data)
    }

    useEffect(() => {
        getResult(id)
    }, [])

    if(!result){
        return null
    }

    return(
        <View style={styles.container}>
            {result.id ?
                <View style={styles.scrollcontainer}>
                    <ScrollView>
                        <StyledText textoTitleElemnt={result.name}/>
                        <View>
                            <StyledText textoElemnt={'  Expansion: ' + result.expansion}/>
                            <StyledText textoElemnt={'  Description: ' + result.description}/>
                            <StyledText textoElemnt={'  Age: ' + result.age}/>
                            <StyledText textoElemnt={'  Cost: '}/>
                            {result.cost && result.cost.Wood ?
                                <StyledText textoElemnt={'        o Wood: ' + result.cost.Wood}/>
                            :null}
                            {result.cost && result.cost.Food ?
                                <StyledText textoElemnt={'        o Food: ' + result.cost.Food}/>
                            :null}
                            {result.cost && result.cost.Stone ?
                                <StyledText textoElemnt={'        o Stone: ' + result.cost.Stone}/>
                            :null}
                            {result.cost && result.cost.Gold ?
                                <StyledText textoElemnt={'        o Gold: ' + result.cost.Gold}/>
                            :null}
                            {result.cost && result.cost.info ? 
                                <StyledText textoElemnt={'        o Info: ' + result.cost.info}/>
                            : null}
                            <StyledText textoElemnt={'  Build time: ' + result.build_time}/>
                            {result.reload_time ? 
                                <StyledText textoElemnt={'  Reload time: ' + result.reload_time}/>
                            :null}
                            {result.attack_delay ? 
                                <StyledText textoElemnt={'  Attack delay: ' + result.attack_delay}/>
                            :null}
                            {result.movement_rate ? 
                                <StyledText textoElemnt={'  Movement rate: ' + result.movement_rate}/>
                            :null}
                            {result.line_of_sight ? 
                                <StyledText textoElemnt={'  Line of sight: ' + result.line_of_sight}/>
                            :null}
                            {result.hit_points ? 
                                <StyledText textoElemnt={'  Hit points: ' + result.hit_points}/>
                            :null}
                            {result.range ? 
                                <StyledText textoElemnt={'  Range: ' + result.range}/>
                            :null}
                            {result.attack ? 
                                <StyledText textoElemnt={'  Attack: ' + result.attack}/>
                            :null}
                            {result.armor ? 
                                <StyledText textoElemnt={'  Armor: ' + result.armor}/>
                            :null}
                            {result.attack_bonus ? 
                                <StyledText textoElemnt={'  Attack bonus: '}/>
                            :null}
                            {result.attack_bonus ? 
                                result.attack_bonus.map((_bonus,_index) => {
                                return(
                                    <StyledText key={_index} textoElemnt={'        o ' + _bonus}/>
                                )
                            })
                            :null}
                            {result.armor_bonus ? 
                                <StyledText textoElemnt={'  Build time: '}/>
                            :null}
                            {result.armor_bonus ? 
                                result.armor_bonus.map((_bonus,_index) => {
                                return(
                                    <StyledText key={_index} textoElemnt={'        o ' + _bonus}/>
                                )
                            })
                            :null}
                            {result.search_radius ?
                                <StyledText textoElemnt={'  Search radius: ' + result.search_radius}/>
                            :null}
                            {result.accuracy ?
                                <StyledText textoElemnt={'  Accuracy: ' + result.accuracy}/>
                            :null}
                            {result.blast_radius ?
                                <StyledText textoElemnt={'  Blast radius: ' + result.blast_radius}/>
                            :null}
                        </View> 
                    </ScrollView>
                </View> 
            :
                <View style={[styles.container,styles.vertical]}>
                    <Image source={require('../../assets/wololo.gif')}/>
                    <Text> {`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`} </Text>
                    <ActivityIndicator size="large" color="#EA3109" />
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: "center",
        marginBottom: 40,
        marginHorizontal: 20,
    },
    scrollcontainer:{
        marginTop:80,
        height: '80%',
        marginHorizontal: 20,
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
    vertical: {
        flexDirection: "column",
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default ResultShowScreen