import React, { useState, useEffect } from 'react'
import {View, Text, StyleSheet, ActivityIndicator, Image, ScrollView} from 'react-native'
import ageApi from '../api/ageApi'
import StyledText from "../components/StyledText"

const CivilizationShowScreen = ({ navigation}) =>{
    const [result, setResult] = useState([])
    const id =navigation.getParam('id')
    
    const getResult = async (id) =>{
        const response = await ageApi.get(`/civilization/${id}`)
        setResult(response.data)
    }

    useEffect(() => {
        getResult(id)
    }, [])

    if(!result){
        return null
    }

    return(
        <View style={styles.container}>
            {result.id ?
                <View style={styles.scrollcontainer}>
                    <ScrollView>
                        <StyledText textoTitleElemnt={result.name}/>
                        <View>
                            <StyledText textoElemnt={'  Expansion: ' + result.expansion}/>
                            <StyledText textoElemnt={'  Army type: ' + result.army_type}/>
                            <StyledText textoElemnt={'  Team bonus: ' + result.team_bonus}/>
                            <StyledText textoElemnt={'  Civilization bonus: '}/>
                            {result.civilization_bonus ? 
                                result.civilization_bonus.map((_bonus,_index) => {
                                return(
                                    <StyledText key={_index} textoElemnt={'        o ' + _bonus}/>
                                )
                            })
                            :null}
                        </View>  
                    </ScrollView>  
                </View> 
            :
                <View style={[styles.container,styles.vertical]}>
                    <Image source={require('../../assets/wololo.gif')}/>
                    <Text> {`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`} </Text>
                    <ActivityIndicator size="large" color="#EA3109" />
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: "center",
        marginBottom: 40,
        marginHorizontal: 20,
    },
    scrollcontainer:{
        marginTop:80,
        height: '80%',
        marginHorizontal: 20,
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
    vertical: {
        flexDirection: "column",
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default CivilizationShowScreen