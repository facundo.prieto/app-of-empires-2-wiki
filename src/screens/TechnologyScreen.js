import React, { useState, useEffect } from 'react'
import {View, Text, StyleSheet, ActivityIndicator, Image, ScrollView} from 'react-native'
import ageApi from '../api/ageApi'
import StyledText from "../components/StyledText"

const ResultShowScreen = ({ navigation}) =>{
    const [result, setResult] = useState([])
    const id =navigation.getParam('id')
    
    const getTechnology = async (id) =>{
        const response = await ageApi.get(`/technology/${id}`)
        setResult(response.data)
    }

    useEffect(() => {
        getTechnology(id)
    }, [])

    if(!result){
        return null
    }

    return(
        <View style={styles.container}>
            {result.id ?
                <View style={styles.scrollcontainer}>
                    <ScrollView>
                        <StyledText textoTitleElemnt={result.name}/>
                        <View>
                            <StyledText textoElemnt={'  Expansion: ' + result.expansion}/>
                            <StyledText textoElemnt={'  Description: ' + result.description}/>
                            <StyledText textoElemnt={'  Cost: '}/>
                            {result.cost && result.cost.Wood ?
                                <StyledText textoElemnt={'        o Wood: ' + result.cost.Wood}/>
                            :null}
                            {result.cost && result.cost.Food ?
                                <StyledText textoElemnt={'        o Food: ' + result.cost.Food}/>
                            :null}
                            {result.cost && result.cost.Stone ?
                                <StyledText textoElemnt={'        o Stone: ' + result.cost.Stone}/>
                            :null}
                            {result.cost && result.cost.Gold ?
                                <StyledText textoElemnt={'        o Gold: ' + result.cost.Gold}/>
                            :null}
                            <StyledText textoElemnt={'  Build time: ' + result.build_time}/>
                        </View> 
                    </ScrollView>
                </View> 
            :
                <View style={[styles.container,styles.vertical]}>
                    <Image source={require('../../assets/wololo.gif')}/>
                    <Text> {`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`} </Text>
                    <ActivityIndicator size="large" color="#EA3109" />
                </View>
            }

        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: "center",
        marginBottom: 40,
        marginHorizontal: 20,
    },
    scrollcontainer:{
        marginTop:80,
        height: '80%',
        marginHorizontal: 20,
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
    vertical: {
        flexDirection: "column",
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default ResultShowScreen