import React, { useState, useEffect } from 'react'
import {View, Text, StyleSheet, ScrollView, ActivityIndicator, Image} from 'react-native'
import StructuresList from '../components/StructuresList'
import useStructures from '../hooks/useStructures'

const StructuresScreen = ({navigation}) =>{
    const [searchApi, structures, errorMessage] = useStructures();
    
    return(
        <View style={{flex:1}}>
            {errorMessage ? <Text>{errorMessage}</Text> : null}
            {structures.length > 0 ?
                <StructuresList results={structures} title='STRUCTURES' navigation={navigation}/>
            :
                <View style={[styles.container,styles.vertical]}>
                    <Image source={require('../../assets/wololo.gif')}/>
                    <Text> {`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`} </Text>
                    <ActivityIndicator size="large" color="#EA3109" />
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center"
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 100,
        flex:0.5
    },
    vertical: {
        flexDirection: "column",
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
});    


export default StructuresScreen