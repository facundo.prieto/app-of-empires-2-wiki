import React, { useState, useEffect } from 'react'
import {View, Text, StyleSheet, ScrollView, ActivityIndicator, Image} from 'react-native'
import ResulList from '../components/CivilizationList'
import useResults from '../hooks/useCivilizations'

const SearchScreen = ({navigation}) =>{
    const [searchApi, civilizations, errorMessage] = useResults();
    
    return(
        <View style={{flex:1}}>
            {errorMessage ? <Text>{errorMessage}</Text> : null}
            {civilizations.length > 0 ?
                <ResulList results={civilizations} title='CIVILIZATIONS' navigation={navigation}/>
            :
                <View style={[styles.container,styles.vertical]}>
                    <Image source={require('../../assets/wololo.gif')}/>
                    <Text> {`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`}{`\n`} </Text>
                    <ActivityIndicator size="large" color="#EA3109" />
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center"
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 100,
        flex:0.5
    },
    vertical: {
        flexDirection: "column",
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
});    

export default SearchScreen