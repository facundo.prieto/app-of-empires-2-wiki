# age-2-wiki

Proyecto para Taller de Aplicaciones de Internet Ricas - FIng - UdelaR

Mathias Eiriz

Tatiana Mathon

Facundo Prieto

# Servicio rest utilizado

El servicio que consumimos se trata de la API pública llamada Age Of Empires II API, que se encuentra hosteada en una nube de Heroku:

https://age-of-empires-2-api.herokuapp.com/docs/#/


# Instalación de herramientas

Descargar Node.js desde la página oficial  https://nodejs.org/ e instalar normalmente.

Desde una terminal, instalar las herramientas de la línea de comandos de expo con el comando “npm install --global expo-cli”.

Descargar e instalar la app de cliente Expo Go en el dispositivo de pruebas (emulador o movil personal), desde la Google Play Store o la Apple Store.


# Instalación de dependencias necesarias

Una vez clonado el repositorio (https://gitlab.fing.edu.uy/facundo.prieto/age-2-wiki.git), desde una terminal ubicarse en la carpeta del proyecto y ejecutar los siguientes comandos:

npm install react-navigation

npm install react-navigation-stack

npm install react-native-gesture-handler --save

expo install expo-av

expo install expo-font

expo install expo-blur

# Probar la aplicación

Desde una terminal, ubicado en la carpeta del proyecto, ejecutar el comando "npm start"

Se generará un código QR (en la terminal y en una nueva pestaña del navegador) que debes escanear con la app Expo Go, y se ejecutará la aplicación dentro.


# Mockup
<img src="https://gitlab.fing.edu.uy/facundo.prieto/age-2-wiki/-/raw/master/Mockup.png" alt="Mockup"/>

# Diseño del sistema
<img src="https://gitlab.fing.edu.uy/facundo.prieto/age-2-wiki/-/raw/master/Dise%C3%B1o%20del%20sistema.png" alt="Mockup"/>

